drop table if exists tour, review cascade;


create table tour
(
    name  text PRIMARY KEY,
    price text
    
    
);

create table review
(
    id       int,
    text   text,
    rating    int,
    tour_name text REFERENCES tour
);


insert into tour
values ('Египет', '100'),
       ('Италия', '300');

insert into review (id, text,rating, tour_name)
values (1, 'Здорово!', 5, 'Египет'),
       (2, 'Не очень понравилось.', 3, 'Египет'),
       (3, 'Прекрасно!', 4.5, 'Италия'),
       (4, 'Слишком дорого!', 2, 'Италия');
       
select *
from tour
         join review on name = tour_name;

select name, price, id as review_id, text
from tour
         join review on name = tour_name;
