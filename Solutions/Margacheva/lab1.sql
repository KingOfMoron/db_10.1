drop table if exists delivery cascade;
drop table if exists courier cascade;
drop table if exists "order" cascade;
drop table if exists delivery_to_courier cascade;

create table delivery
(
    id              bigint generated always as identity primary key,
    name            text,
    quisine         text not null,
    delievery_speed text not null
);

create table courier
(
    id        bigint generated always as identity primary key,
    name      text,
    age       int not null,
    transport text
);

create table "order"
(
    id   bigint generated always as identity primary key,
    price      int         not null,
    time       timestamptz not null,
    courier_id int references courier
);

create table delivery_to_courier
(
    delivery_id int references delivery,
    courier_id  int references courier,
    primary key (delivery_id, courier_id)
);

insert into delivery(name, quisine, delievery_speed)
values ('Delivery Club', 'mixed', '60 - 70 min'),
       ('Yandex', 'chinese', '90-140 min'),
       ('Scubbie Doo', 'American', '60-90 min'),
       ('Dostaevski', 'mixed', '90-140 min'),
       ('Fasty', 'mixed', '90-140 min'),
       ('Yanz', 'Japanese', '90-140 min'),
       ('3Dex', 'Chinese', '90-140 min'),
       ('Puro', 'Russian', '90-140 min'),
       ('Bistro', 'Chinese', '90 min'),
       ('ProKitchen', 'mixed', '140 min'),
       ('Goglet', 'chinese', '100 min'),
       ('Yandex', 'chinese', '90-140 min');
insert into courier(name, age, transport)
values ('Oleg', 23, 'no'),
       ('Yana', 24, 'yes'),
       ('Max', 20, 'no'),
       ('Alice', 30, 'yes'),
       ('Nikita', 27, 'yes'),
       ('Ann', 27, 'no'),
       ('Kate', 22, 'yes'),
       ('Lev', 29, 'yes'),
       ('Kirill', 20, 'yes'),
       ('Ann', 34, 'yes'),
       ('Andrew', 27, 'yes'),
       ('John', 45, 'no');
insert into "order"(price, time, courier_id)
values (450, '2022-11-12 12:59:59+03', 1),
       (500, '2022-11-12 11:59:59+03', 2),
       (670, '2022-11-12 22:59:59+03', 3),
       (1430, '2022-11-12 21:59:59+03', 4),
       (1400, '2022-11-12 21:59:59+03', 5),
       (1240, '2022-11-12 21:59:59+03', 6),
       (750, '2022-11-12 21:59:59+03', 7),
       (4500, '2022-11-12 21:59:59+03', 8),
       (3200, '2022-11-12 21:59:59+03', 9),
       (1270, '2022-11-12 21:59:59+03', 10),
       (1400, '2022-11-12 21:59:59+03', 11),
       (3000, '2022-11-12 13:59:59+03', 12);
select *
from delivery;
select *
from courier;
select *
from "order";