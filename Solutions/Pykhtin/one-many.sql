drop table if exists tour, rewiew cascade;



create table tour
(
	price numeric,
	name text primary key
);

create table rewiew
(
	id int,
	comment text,	
	rating int,
	tour_name text references tour
);

insert into tour 
values ('10000', 'Сочи'),
		('15000','Крым');

insert into rewiew (id, comment, rating, tour_name)
values (101,'Хорошо',8,'Сочи'),
		(101, 'Нормально', 7, 'Сочи'),
		(202, 'Отлично', 10, 'Крым'),
		(202, 'Плохо', 2, 'Крым');

select name, price, id as equipment_id, comment, rating from tour 
join rewiew on name = tour_name