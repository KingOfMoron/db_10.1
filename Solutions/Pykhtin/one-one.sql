drop table if exists users, settings cascade;



create table users 
(
	username text primary key,
	first_name text,
	last_name text
);

create table settings 
(
	font_size int,
	color_scheme text,
	user_nickname text references users unique
);



insert into users 
values ('Alpha', 'Alex', 'Pykhtin'),
		('Beta', 'Immanuin', 'Larin');
		
insert into settings (font_size, color_scheme, user_nickname)
values ('12', 'Blue', 'Alpha'),
		('14', 'White', 'Beta');
	
select *
from users
         join settings on username = user_nickname;

        
select username, first_name, last_name, font_size, color_scheme
from users
         join settings on username = user_nickname;	

		
