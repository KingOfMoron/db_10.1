drop table if exists users, settings cascade;

create table users
(
    nickname  text primary key,
    firstname text,
    lastname text
);

create table settings
(
    font_size       int,
    color_scheme    text,
    user_nickname text references users unique
);


insert into users
values ('ivanov@', 'Иван', 'Иванов'),
       ('petrov@', 'Петр', 'Петров');
    
insert into settings (font_size, color_scheme, user_nickname)
values (10, 'белый', 'ivanov@'),
       (11, 'черный', 'petrov@');
       

select nickname, firstname, lastname, font_size, color_scheme
from users
         join settings on nickname = user_nickname;
