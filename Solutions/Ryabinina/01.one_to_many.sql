drop table if exists tour, review cascade;

create table tour
(
	price numeric,
	name text primary key
);

create table review
(
	id int,
	comment text,
	rating int,
	tour_name text references tour
);

insert into tour
values (100, 'Мальдивы'),
(120, 'Сейшелы');

insert into review (id, comment, rating, tour_name)
values (1, 'Отлично', 7, 'Сейшелы'),
(2, 'Хорошо', 5, 'Мальдивы'),
(3, 'Класс', 6, 'Сейшелы'),
(4, 'Нормально', 4, 'Мальдивы');


select name, price, id as review_id, comment, rating from tour
join review on name = tour_name;
